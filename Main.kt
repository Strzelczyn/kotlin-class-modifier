open class Animal(age: Int, weight: Double) {
    protected var age: Int = age
    protected var weight: Double = weight
}

class Dog(years: Int, kg: Double) : Animal(years, kg) {
    var ageDog: Int = age
    var weightDog: Double = weight
    fun showAgeOfDog() {
        println("Dog is $age years old.")
    }
}

private class Cat(years: Int, kg: Double) : Animal(years, kg) {
    var ageCat: Int = age
    var weightCat: Double = weight
    fun showAgeOfCat() {
        println("Cat is $age years old.")
    }

    fun withCatIsOlder(catToCompre: Cat) {
        if (this.ageCat > catToCompre.ageCat) {
            println("First cat is older.")
            this.showAgeOfCat()
        } else {
            println("Second cat is older.")
            catToCompre.showAgeOfCat()
        }
    }
}

internal class Horse(years: Int, kg: Double) : Animal(years, kg) {
    var ageHorse: Int = age
    var weightHorse: Double = weight
    fun showAgeOfHorse() {
        println("Horse is $age years old.")
    }
}

fun main(args: Array<String>) {
    var firstDog = Dog(1, 2.0)
    var secondDog = Dog(2, 3.0)
    withDogIsOlder(firstDog, secondDog)
    var firstCat = Cat(8, 3.2) // object can be only created inside file with this class
    var secondCat = Cat(5, 4.2)
    firstCat.withCatIsOlder(secondCat)
    var firstHorse = Horse(12, 300.0) // visible inside in one module
}

fun withDogIsOlder(left: Dog, right: Dog) {
    if (left.ageDog > right.ageDog) {
        println("First dog is older.")
        left.showAgeOfDog()
    } else {
        println("Second dog is older.")
        right.showAgeOfDog()
    }
}
